'use strict';

var openesbNotifications = angular.module('openesb.notifications', [
        'ui.router'
    ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $stateProvider
                    .state('notifications', {
                        url: '/notifications',
                        templateUrl: 'views/notifications/notifications-list.html',
                        controller: 'NotificationsListCtrl'
                    });
            }
        ]
    );

openesbNotifications.controller('NotificationsListCtrl', ['$scope', '$q', '$http', '$timeout',
    function ($scope, $q, $http, $timeout) {

    }
]);