'use strict';

openesbApp.controller('HomeCtrl', ['$scope', '$route', '$http', '$q','$location','InstanceService',
    function($scope, $route, $http, $q,$location, InstanceService) {

        var instance = $http.get('/');
        var assemblies = $http.get('/assemblies');
        var components = $http.get('/components');
        var libraries = $http.get('/libraries');
        var endpoints = $http.get('/nmr');
        var jvm = $http.get('/jvm');
        var memory = $http.get('/jvm/memory');

        $q.all([instance, assemblies, components, libraries, endpoints, jvm, memory]).then(function(values) {
            $scope.informations = values[0].data;
            $scope.assembliesCount = values[1].data.length;
            $scope.componentsCount = values[2].data.length;
            $scope.librariesCount = values[3].data.length;
            $scope.endpointsCount = values[4].data.endpointCount;
            $scope.jvm = values[5].data;
            $scope.memory = values[6].data;
        });
    }
]);