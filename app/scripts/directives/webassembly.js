/**
@toc

@param {Object} scope (attrs that must be defined on the scope (i.e. in the controller) - they can't just be defined in the partial html). REMEMBER: use snake-case when setting these on the partial!
TODO

@param {Object} attrs REMEMBER: use snake-case when setting these on the partial! i.e. my-attr='1' NOT myAttr='1'
TODO

@dependencies
TODO

@usage
partial / html:
TODO

controller / js:
TODO

//end: usage
*/

'use strict';

openesbApp.directive('webAssembly', ['$timeout',  function ($timeout) {

	return {
		restrict: 'E',
		scope: {
			map: '=map',
			loading: '@'
		},

		replace: true,

		templateUrl: 'views/directives/viewer/map.html',

		link: function(scope, element, attrs) {

			scope.shortname = function(tab) {
				$(".map-loader").show();//@remove test
				return TemplatesUtils.getComponentShortName(tab);
			}  

			scope.finish = function(){
				//scope.loading = false;
				scope.$emit('loading');
			}

            var rendered = false;
			scope.$watch('map', function(newValue, oldValue) {
                if (newValue){
                	scope.$emit('loaded');
                    element.ready(function(){

                    	if(rendered == false){
                    		rendered = true;
		                    	$timeout(function() {

									var renderer = new SADisplay();
									renderer.init({
										container: "#webassembly"
									});
									renderer.display(scope.map);

		                    	}, 150);
						}
					
						scope.finish();
					});
                }
            });

		}
	};
}]);