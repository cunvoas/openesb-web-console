'use strict';

openesbApp.directive('fieldDirective', ['$http', '$compile', function ($http, $compile) {
    var getTemplateUrl = function(field) {
        var type = field.field_type;
        var templateUrl = '';

        switch (type) {
            case 'number':
                templateUrl = 'views/directives/field/number.html';
                break;
            case 'textfield':
                templateUrl = 'views/directives/field/textfield.html';
                break;
            case 'email':
                templateUrl = 'views/directives/field/email.html';
                break;
            case 'textarea':
                templateUrl = 'views/directives/field/textarea.html';
                break;
            case 'checkbox':
                templateUrl = 'views/directives/field/checkbox.html';
                break;
            case 'date':
                templateUrl = 'views/directives/field/date.html';
                break;
            case 'dropdown':
                templateUrl = 'views/directives/field/dropdown.html';
                break;
            case 'hidden':
                templateUrl = 'views/directives/field/hidden.html';
                break;
            case 'password':
                templateUrl = 'views/directives/field/password.html';
                break;
            case 'radio':
                templateUrl = 'views/directives/field/radio.html';
                break;
        }
        return templateUrl;
    };

    var linker = function(scope, element) {
        // GET template content from path
        var templateUrl = getTemplateUrl(scope.field);
        $http.get(templateUrl).success(function(data) {
            element.html(data);
            $compile(element.contents())(scope);
        });
    };

    return {
        template: '<div>{{field}}</div>',
        restrict: 'E',
        scope: {
            field: '='
        },
        link: linker
    };
}]);