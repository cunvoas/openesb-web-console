'use strict';

openesbApp.service('InstanceService', ['$http', '$q', 'Datastore', 'toaster',
    function($http, $q, Datastore, toaster) {
        this.getInformations = function() {
            return $http.get('/');
        };

        this.getJVMMetrics = function() {
            return $q.all([
                $http.get('/jvm/gc'),
                $http.get('/jvm/memory'),
                $http.get('/jvm/thread')]);
        };

        this.getStatistics = function() {
            return $http.get('/nmr/stats');
        };

        this.getLoggers = function() {
            return $http.get('/loggers');
        };

        this.setLoggerLevel = function(loggerName, loggerLevel) {
            return $http.put('/loggers?logger=' + loggerName + '&level=' + loggerLevel);
        };

        this.checkAvailability = function() {
            this.getInformations()
                .error(function(data) {
                    var server = Datastore.getCurrentInstance().url;

                    toaster.pop('error', 'Instance not available', 'Could not contact OpenESB instance at '+server+
                        '. Please ensure that OpenESB is reachable from your system.', 5000);
                });
        }
    }
]);