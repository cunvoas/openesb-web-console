'use strict';

openesbApp.service('AssemblyService', ['$http', '$q',
    function($http, $q) {
        this.findAll = function() {
            return $http.get('/assemblies');
        };

        this.get = function(assemblyName) {
            return $http.get('/assemblies/' + assemblyName);
        };

        this.getDescriptorAsXml = function(assemblyName) {
            var fullResult = $q.defer();

            $http.get(
                '/assemblies/' + assemblyName + '/descriptor',
                {
                    headers: {'Accept': 'application/xml'}
                }
            ).success(function(response) {
                    var descriptor = {};
                    descriptor.xml = _.escape(response.toString());
                    fullResult.resolve(descriptor);
            });

            return fullResult.promise;
        };

        this.start = function(assemblyName) {
            return $http.post('/assemblies/' + assemblyName + '?action=start');
        };

        this.stop = function(assemblyName) {
            return $http.post('/assemblies/' + assemblyName + '?action=stop');
        };

        this.shutdown = function(assemblyName) {
            return $http.post('/assemblies/' + assemblyName + '?action=shutdown');
        };

        this.delete = function(assemblyName) {
            return $http.delete('/assemblies/' + assemblyName + '?force=true');
        }

        this.getStatistics = function(assemblyName) {
            return $http.get('/assemblies/' + assemblyName + '/stats');
        };

        this.getMap = function(assemblyName) {
            return $http.get('/ui/map/' + assemblyName);
        };

        this.startAssemblies = function(assemblies) {
          return this.doGlobalAction('start', assemblies);
        };

        this.stopAssemblies = function(assemblies) {
          return this.doGlobalAction('stop', assemblies);
        };

        this.shutdownAssemblies = function(assemblies) {
          return this.doGlobalAction('shutdown', assemblies);
        };

        this.doGlobalAction = function(action, assemblies) {
          var result = $q.defer();
          var promises = [];

          for(var i = 0 ; i < assemblies.length ; i++) {
            promises.push($http.post('/assemblies/' + assemblies[i] + '?action=' + action));
          }

          $q.all(promises).then(function(data){
            result.resolve(data);
          }, function(error) {
            result.reject(error);
          });

          return result.promise;
        }
    }
]);
